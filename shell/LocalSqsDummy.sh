#!/bin/bash

cd /home/vagrant/workspace/LocalSqsDummy

commons_codec=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-codec/commons-codec/1.4/5310da9f90e843883309e9e0bf5950faa79882a0/commons-codec-1.4-sources.jar`
slf4j=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.slf4j/jcl-over-slf4j/1.7.12/1f2ab7827a222427f8b8f20ee703fd5c5cfc055e/jcl-over-slf4j-1.7.12-sources.jar`
log4j=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.slf4j/log4j-over-slf4j/1.7.12/89ed3e254ab429d35257f6bd48cd28cebc0b2a45/log4j-over-slf4j-1.7.12-sources.jar`
slf4j_2=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.slf4j/jul-to-slf4j/1.7.12/9bc85314566e28525c79697d52a6bfb81d1b998a/jul-to-slf4j-1.7.12-sources.jar`
spring_test=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-test/4.3.3.RELEASE/155897ee1c39fb49a219446740cbdc38ac9fc23e/spring-test-4.3.3.RELEASE-sources.jar`
commons_pool=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.apache.commons/commons-pool2/2.3/9e03327e766c3d16ed34a617818ab082192e83a7/commons-pool2-2.3-sources.jar`
commons_collection=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-collections/commons-collections/3.2.1/fa095ef874374e5b2a11f8b06c26a5d68c7cb3a4/commons-collections-3.2.1-sources.jar`
commons_lang=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-lang/commons-lang/2.4/2b8c4b3035e45520ef42033e823c7d33e4b4402c/commons-lang-2.4-sources.jar`
commons_digester=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-digester/commons-digester/1.8/6c296de7dc352e0af9a40f92f5af995314d41fc9/commons-digester-1.8-sources.jar`
commons_beanutils_core=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-beanutils/commons-beanutils-core/1.8.0/175dc721f87e4bc5cc0573f990e28c3cf9117508/commons-beanutils-core-1.8.0.jar`
spring_beans=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-beans/4.3.3.RELEASE/8c7e14b6a9c4d41a5b22c008acb4931144d33785/spring-beans-4.3.3.RELEASE-sources.jar`
spring_core=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-core/4.3.3.RELEASE/ee672807fd6afe5ab70e1d089e38ed925d527a36/spring-core-4.3.3.RELEASE-sources.jar`
spring_tx=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-tx/4.3.3.RELEASE/d1eb31cb1b600bb0fb023e3b05b9302cec7e331e/spring-tx-4.3.3.RELEASE-sources.jar`
spring_aop=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-aop/4.3.3.RELEASE/c76807073b04f9ba788f372aa359d759d9eaefb4/spring-aop-4.3.3.RELEASE-sources.jar`
spring_context=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-context/4.3.3.RELEASE/56718cbc3332feeacfc988358d4064510a13a06d/spring-context-4.3.3.RELEASE-sources.jar`
spring_expression=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-expression/4.3.3.RELEASE/c8aac0843b6a02ed04cae2346a7ccc81e5ebfea8/spring-expression-4.3.3.RELEASE-sources.jar`
spring_web=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.springframework/spring-web/4.3.3.RELEASE/f963d559a56e848d01f973e4eb8c2428b6d8a6cd/spring-web-4.3.3.RELEASE-sources.jar`
hamcrest=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.hamcrest/hamcrest-core/1.1/2ccf1154d1a8936042a8a742dc3e611d02ac7213/hamcrest-core-1.1-sources.jar`
commons_beanutils=`/home/vagrant/.gradle/caches/modules-2/files-2.1/commons-beanutils/commons-beanutils/1.7.0/b68c4fc66026e8c08df7fb57c7dc1e94a6ed8cbb/commons-beanutils-1.7.0-sources.jar`
javassist=`/home/vagrant/.gradle/caches/modules-2/files-2.1/org.javassist/javassist/3.19.0-GA/37e6508aeea3eef21df5b7ee3ea3c708618c1b7b/javassist-3.19.0-GA-sources.jar`

javac -cp ${commons_codec}:${slf4j}:${log4j}:${slf4j_2}:${spring_test}:${commons_pool}:${commons_collection}:${commons_lang}:${commons_digester}:${commons_beanutils_core}:${spring_beans}:${spring_core}:${spring_tx}:${spring_aop}:${spring_context}:${spring_expression}:${spring_web}:${hamcrest}:${powermock_api}:${commons_beanutils}:${javassist} -d class ~/workspace/LocalSqsDummy/src/jp/co/dummy/main/*.java ~/workspace/LocalSqsDummy/src/jp/co/dummy/util/*.java ~/workspace/LocalSqsDummy/src/jp/co/dummy/base/*.java ~/workspace/LocalSqsDummy/src/jp/co/dummy/constant/*.java ~/workspace/LocalSqsDummy/src/jp/co/dummy/dao/*.java ~/workspace/LocalSqsDummy/src/jp/co/dummy/regist/*.java

cd /home/vagrant/workspace/LocalSqsDummy/class/jp/co/dummy/main/
java LocalDummyData


