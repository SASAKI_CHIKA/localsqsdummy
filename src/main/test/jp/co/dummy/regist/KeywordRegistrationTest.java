package main.test.jp.co.dummy.regist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.regist.KeywordRegistration;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

public class KeywordRegistrationTest {

  /**
   * キーワード登録の場合.
   */
  @Test
  public void dbRegistTest() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String keyword = "test";
    boolean errorFlag = false;

    // 既存クリエイティブID取得
    String sql = "select id from t_creative ORDER BY id desc LIMIT 1;";
    int adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);
    KeywordRegistration regist = new KeywordRegistration(id, keyword);

    try {
      regist.registDelivery(id, keyword);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * クリエイティブIDがnullの場合.
   */
  @Test
  public void registDeliveryTest2() {
    String keyword = "test";
    KeywordRegistration regist = new KeywordRegistration(null, keyword);
    boolean errorFlag = false;
    try {
      regist.registDelivery(null, keyword);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * キーワードがnullの場合.
   */
  @Test
  public void registDeliveryTest3() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    boolean errorFlag = false;

    // 既存クリエイティブID取得
    String sql = "select id from t_creative ORDER BY id desc LIMIT 1;";
    int adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);
    KeywordRegistration regist = new KeywordRegistration(id, null);

    try {
      regist.registDelivery(id, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * クリエイティブID、キーワード共にnullの場合.
   */
  @Test
  public void registDeliveryTest4() {
    KeywordRegistration regist = new KeywordRegistration(null, null);
    boolean errorFlag = false;
    try {
      regist.registDelivery(null, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
