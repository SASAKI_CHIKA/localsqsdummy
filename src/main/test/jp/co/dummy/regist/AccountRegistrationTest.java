package main.test.jp.co.dummy.regist;

import static org.junit.Assert.*;
import jp.co.dummy.regist.AccountRegistration;

import org.junit.Test;

public class AccountRegistrationTest {

  /**
   * アカウントからキーワードまで登録する場合.
   */
  @Test
  public void registDeliveryTest() {
    AccountRegistration regist = new AccountRegistration();
    boolean errorFlag = false;
    try {
      regist.registDelivery();
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * アカウント登録の場合.
   */
  @Test
  public void registDeliveryTest2() {
    AccountRegistration regist = new AccountRegistration();
    String accountName = "test";
    boolean errorFlag = false;
    try {
      regist.registDelivery(accountName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * アカウント名がnullの場合.
   */
  @Test
  public void registDeliveryTest3() {
    AccountRegistration regist = new AccountRegistration();
    String accountName = null;
    boolean errorFlag = false;
    try {
      regist.registDelivery(accountName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }
}
