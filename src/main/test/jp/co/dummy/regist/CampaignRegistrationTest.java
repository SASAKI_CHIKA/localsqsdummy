package main.test.jp.co.dummy.regist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.regist.CampaignRegistration;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

public class CampaignRegistrationTest {

  /**
   * キャンペーン登録の場合.
   */
  @Test
  public void registDeliveryTest() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String campaignName = "test";
    boolean errorFlag = false;

    // 既存のアカウントID取得
    String sql = "select id from t_account ORDER BY id desc LIMIT 1;";
    int accountId = vg.getNewId(sql);
    String id = vc.changeString(accountId - 1);
    CampaignRegistration regist = new CampaignRegistration(id, campaignName);

    try {
      regist.registDelivery(id, campaignName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * アカウントIDがnullの場合.
   */
  @Test
  public void registDeliveryTest2() {
    String campaignName = "test";
    boolean errorFlag = false;
    CampaignRegistration regist = new CampaignRegistration(null, campaignName);

    try {
      regist.registDelivery(null, campaignName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * キャンペーン名がnullの場合.
   */
  @Test
  public void registDeliveryTest3() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String campaignName = null;
    boolean errorFlag = false;

    // 既存のアカウントID取得
    String sql = "select id from t_account ORDER BY id desc LIMIT 1;";
    int accountId = vg.getNewId(sql);
    String id = vc.changeString(accountId - 1);
    CampaignRegistration regist = new CampaignRegistration(id, campaignName);

    try {
      regist.registDelivery(id, campaignName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * アカウントID、キャンペーン名共にnullの場合.
   */
  @Test
  public void registDeliveryTest4() {
    boolean errorFlag = false;
    CampaignRegistration regist = new CampaignRegistration(null, null);

    try {
      regist.registDelivery(null, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
