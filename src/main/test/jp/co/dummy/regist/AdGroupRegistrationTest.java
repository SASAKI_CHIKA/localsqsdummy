package main.test.jp.co.dummy.regist;

import static org.junit.Assert.*;
import jp.co.dummy.regist.AdGroupRegistration;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

public class AdGroupRegistrationTest {

  /**
   * 広告グループ登録の場合.
   */
  @Test
  public void registDeliveryTest() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String adGroupName = "test";
    int campaignId;
    boolean errorFlag = false;

    //既存のキャンペーンID取得
    String sql = "select id from t_campaign ORDER BY id desc LIMIT 1;";
    campaignId = vg.getNewId(sql);
    String id = vc.changeString(campaignId - 1);
    AdGroupRegistration regist = new AdGroupRegistration(id, adGroupName);
    
    try {
      regist.registDelivery(id, adGroupName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * キャンペーンIDがnullの場合.
   */
  @Test
  public void registDeliveryTest2() {
    String adGroupName = "test";
    boolean errorFlag = false;
    AdGroupRegistration regist = new AdGroupRegistration(null, adGroupName);
    try {
      regist.registDelivery(null, adGroupName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * 広告グループ名がnullの場合.
   */
  @Test
  public void registDeliveryTest3() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int campaignId;
    String adGroupName = null;
    boolean errorFlag = false;

    //既存のキャンペーンID取得
    String sql = "select id from t_campaign ORDER BY id desc LIMIT 1;";
    campaignId = vg.getNewId(sql);
    String id = vc.changeString(campaignId - 1);
    AdGroupRegistration regist = new AdGroupRegistration(id, adGroupName);
    try {
      regist.registDelivery(id, adGroupName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }
}
