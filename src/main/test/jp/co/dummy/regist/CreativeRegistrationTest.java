package main.test.jp.co.dummy.regist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.regist.CreativeRegistration;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

public class CreativeRegistrationTest {

  /**
   * クリエイティブ登録の場合.
   */
  @Test
  public void registDeliveryTest() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String creativeName = "test";
    boolean errorFlag = false;

    String sql = "select id from t_ad_group ORDER BY id desc LIMIT 1;";
    int adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);
    CreativeRegistration regist = new CreativeRegistration(id, creativeName);

    try {
      regist.registDelivery(id, creativeName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * 広告グループIDがnullの場合.
   */
  @Test
  public void registDeliveryTest2() {
    String creativeName = "test";
    boolean errorFlag = false;
    CreativeRegistration regist = new CreativeRegistration(null, creativeName);

    try {
      regist.registDelivery(null, creativeName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * クリエイティブ名がnullの場合.
   */
  @Test
  public void registDeliveryTest3() {
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    String creativeName = null;
    boolean errorFlag = false;

    String sql = "select id from t_ad_group ORDER BY id desc LIMIT 1;";
    int adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);
    CreativeRegistration regist = new CreativeRegistration(id, creativeName);

    try {
      regist.registDelivery(id, creativeName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * 広告グループID,クリエイティブ名共にnullの場合.
   */
  @Test
  public void registDeliveryTest4() {
    boolean errorFlag = false;
    CreativeRegistration regist = new CreativeRegistration(null, null);

    try {
      regist.registDelivery(null, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
