package main.test.jp.co.dummy.regist;

import static org.junit.Assert.*;
import jp.co.dummy.regist.MasterCsvReader;

import org.junit.Test;

public class MasterCsvReaderTest {

  /**
   * マスターデータ全登録の場合.
   */
  @Test
  public void masterCsvReaderTest() {
    boolean errorFlag = false;
    MasterCsvReader regist = new MasterCsvReader();
    try {
      regist.dbRegist();
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * マスターデータ指定登録の場合.
   */
  @Test
  public void masterCsvReaderTest2() {
    String csvName = "m_menu.csv";
    boolean errorFlag = false;
    MasterCsvReader regist = new MasterCsvReader(csvName);
    try {
      regist.registDelivery(csvName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * マスターデータ名null指定の場合
   */
  @Test
  public void masterCsvReaderTest3() {
    boolean errorFlag = false;
    MasterCsvReader regist = new MasterCsvReader(null);
    try {
      regist.registDelivery(null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
