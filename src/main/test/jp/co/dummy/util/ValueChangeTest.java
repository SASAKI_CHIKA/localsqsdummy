package main.test.jp.co.dummy.util;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;

import jp.co.dummy.util.ValueChange;

import org.junit.Test;

/**
 * 値の変換処理テスト.
 * 
 * @author vagrant
 *
 */
public class ValueChangeTest {

  /**
   * 数値123を文字列変換する場合.
   */
  @Test
  public void changeStringTest() {
    ValueChange vc = new ValueChange();
    int figure = 123;
    String result = vc.changeString(figure);
    assertEquals("123", result);
  }

  /**
   * 文字列"123"を数値変換する場合.
   */
  @Test
  public void changeFigureTest() {
    ValueChange vc = new ValueChange();
    String str = "123";
    int result = vc.changeFigure(str);
    assertEquals(123, result);
  }

  /**
   * nullを入れた場合.
   */
  @Test
  public void changeFigureTest2() {
    ValueChange vc = new ValueChange();
    String str = null;
    boolean errorFlag = false;
    try {
      vc.changeFigure(str);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * 文字列"123"をハッシュ化する場合.
   * 
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void hashTest() throws NoSuchAlgorithmException {
    ValueChange vc = new ValueChange();
    String text = "123";
    String result = null;
    result = vc.hash(text);
    assertEquals("202cb962ac59075b964b07152d234b70", result);
  }

  /**
   * nullを入れた場合.
   * 
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void hashTest2() throws NoSuchAlgorithmException {
    ValueChange vc = new ValueChange();
    String text = null;
    String result = null;
    boolean errorFlag = false;
    try {
      result = vc.hash(text);
      assertNull(result);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
