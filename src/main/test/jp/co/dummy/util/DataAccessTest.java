package main.test.jp.co.dummy.util;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import jp.co.dummy.util.DataAccess;

import org.apache.commons.dbcp2.BasicDataSource;
import org.junit.Test;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * DB接続テスト.
 * 
 * @author vagrant
 *
 */
public class DataAccessTest {

  /**
   * DB接続.
   */
  @Test
  public void dataSourceTest() {
    DataAccess db = new DataAccess();
    DataSource dataSource;
    dataSource = db.dataSource();
    assertTrue(dataSource instanceof BasicDataSource);
  }

  /**
   * トランザクション制御.
   */
  @Test
  public void transactionManagerTest() {
    DataAccess db = new DataAccess();
    DataSource dataSource;
    dataSource = db.dataSource();
    PlatformTransactionManager txManager;

    txManager = db.transactionManager(dataSource);
    assertTrue(txManager instanceof DataSourceTransactionManager);
  }
}
