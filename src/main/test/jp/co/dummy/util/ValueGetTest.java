package main.test.jp.co.dummy.util;

import static org.junit.Assert.*;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

/**
 * 値の取得テスト.
 * 
 * @author vagrant
 *
 */
public class ValueGetTest {

  /**
   * 新規アカウントID取得の場合.
   */
  @Test
  public void getNewIdTest() {
    ValueGet vg = new ValueGet();
    String sql = "SELECT id FROM t_account ORDER BY id desc LIMIT 1;";
    int result = vg.getNewId(sql);
    System.out.println(result);
    assertNotNull(result);
  }

  /**
   * nullの場合
   */
  @Test
  public void getNewIdTest2() {
    ValueGet vg = new ValueGet();
    String sql = null;
    int result = vg.getNewId(sql);
    assertEquals(1, result);
  }

  /**
   * ログインID取得の場合.
   */
  @Test
  public void getLoginIdTest() {
    ValueGet vg = new ValueGet();
    String result = vg.getLoginId();
    assertNotNull(result);
  }
}
