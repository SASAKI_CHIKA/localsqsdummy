package main.test.jp.co.dummy.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.dummy.regist.AccountRegistration;
import jp.co.dummy.regist.AdGroupRegistration;
import jp.co.dummy.regist.CampaignRegistration;
import jp.co.dummy.regist.CreativeRegistration;
import jp.co.dummy.regist.KeywordRegistration;
import jp.co.dummy.regist.MasterCsvReader;
import jp.co.dummy.util.ArgsJudge;

import org.junit.Test;

public class ArgsJudgeTest {

  /**
   * 引数が入っている場合.
   */
  @Test
  public void argsJudgeTest() {
    ArgsJudge judge = new ArgsJudge();
    boolean result = judge.argsJudge("test");
    assertFalse(result);
  }

  /**
   * 引数がnullの場合.
   */
  @Test
  public void argsJudgeTest2() {
    ArgsJudge judge = new ArgsJudge();
    boolean result = judge.argsJudge(null);
    assertTrue(result);
  }

  /**
   * 引数masterの場合.
   */
  @Test
  public void createInstanceTest() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("master");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof MasterCsvReader);
  }

  /**
   * 引数csvファイル指定の場合.
   */
  @Test
  public void createInstanceTest2() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("m_menu.csv");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof MasterCsvReader);
  }

  /**
   * 引数allの場合.
   */
  @Test
  public void createInstanceTest3() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("all");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof AccountRegistration);
  }

  /**
   * アカウント登録の場合(アカウント名指定なし).
   */
  @Test
  public void createInstanceTest4() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("account");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof AccountRegistration);
  }

  /**
   * アカウント登録の場合.
   */
  @Test
  public void createInstanceTest5() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("account");
    list.add("test");
    obj = judge.createInstance(list);
    assertNotNull(obj);
  }

  /**
   * キャンペーン登録の場合(キャンペーン名指定なし).
   */
  @Test
  public void createInstanceTest6() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("campaign");
    list.add("1");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof CampaignRegistration);
  }

  /**
   * キャンペーン登録の場合.
   */
  @Test
  public void createInstanceTest7() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("campaign");
    list.add("1");
    list.add("test");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof CampaignRegistration);
  }
  
  /**
   * 広告グループ登録の場合(グループ名指定なし).
   */
  @Test
  public void createInstanceTest8() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("ad_group");
    list.add("1");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof AdGroupRegistration);
  }
  
  /**
   * 広告グループ登録の場合.
   */
  @Test
  public void createInstanceTest9() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("ad_group");
    list.add("1");
    list.add("test");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof AdGroupRegistration);
  }
  
  /**
   * クリエイティブ登録(クリエイティブ名指定なし)の場合.
   */
  @Test
  public void createInstanceTest10() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("creative");
    list.add("1");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof CreativeRegistration);
  }
  
  /**
   * クリエイティブ登録の場合.
   */
  @Test
  public void createInstanceTest11() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("creative");
    list.add("1");
    list.add("test");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof CreativeRegistration);
  }
  
  /**
   * キーワード登録(キーワード名指定なし)の場合.
   */
  @Test
  public void createInstanceTest12() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("keyword");
    list.add("1");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof KeywordRegistration);
  }
  
  /**
   * キーワード登録(キーワード名指定なし)の場合.
   */
  @Test
  public void createInstanceTest13() {
    ArgsJudge judge = new ArgsJudge();
    Object obj = null;
    List<String> list = new ArrayList<String>();
    list.add("keyword");
    list.add("1");
    list.add("test");
    obj = judge.createInstance(list);
    assertTrue( obj instanceof KeywordRegistration);
  }
}
