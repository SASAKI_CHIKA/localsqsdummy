package main.test.jp.co.dummy.dao;

import static org.junit.Assert.*;

import java.util.Properties;

import jp.co.dummy.dao.PropertiesLoad;

import org.junit.Test;

/**
 * プロパティのロードテスト.
 * 
 * @author vagrant
 *
 */
public class PropertiesLoadTest {

  /**
   * プロパティのロード
   */
  @Test
  public void propertyLoadTest() {
    PropertiesLoad pl = new PropertiesLoad();
    Properties properties;
    properties = pl.propertyLoad();
    assertNotNull(properties);
  }

}
