package main.test.jp.co.dummy.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.dao.CreativeDao;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

/**
 * クリエイティブ登録処理テスト.
 * 
 * @author vagrant
 *
 */
public class CreativeDaoTest {

  /**
   * 適切な広告グループID、クリエイティブ名を入れた場合.
   */
  @Test
  public void creativeRegistTest1() {
    CreativeDao dao = new CreativeDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int adGroupId;
    String creativeName = "test";

    String sql = "SELECT id FROM t_ad_group group by id desc";
    adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);

    int creativeId = dao.dbRegist(id, creativeName);
    assertNotNull(creativeId);
  }

  /**
   * 広告グループIDがnullの場合.
   */
  @Test
  public void creativeRegistTest2() {
    CreativeDao dao = new CreativeDao();
    String creativeName = "test";
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, creativeName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * クリエイティブ名がnullの場合.
   */
  @Test
  public void creativeRegistTest3() {
    CreativeDao dao = new CreativeDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int adGroupId;

    String sql = "SELECT id FROM t_ad_group group by id desc";
    adGroupId = vg.getNewId(sql);
    String id = vc.changeString(adGroupId - 1);

    int creativeId = dao.dbRegist(id, null);
    assertNotNull(creativeId);

  }

  /**
   * クリエイティブID,キーワード共にnull
   */
  @Test
  public void creativeRegistTest4() {
    CreativeDao dao = new CreativeDao();
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, null);

    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

}
