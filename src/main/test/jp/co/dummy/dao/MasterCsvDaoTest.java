package main.test.jp.co.dummy.dao;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.util.Properties;

import jp.co.dummy.dao.MasterCsvDao;
import jp.co.dummy.dao.PropertiesLoad;

import org.junit.Ignore;
import org.junit.Test;

/**
 * マスターデータ登録処理テスト.
 * 
 * @author vagrant
 *
 */
public class MasterCsvDaoTest {

  /**
   * マスターデータ全登録の場合.
   */
  @Test
  public void masterCsvAllTest() {
    MasterCsvDao dao = new MasterCsvDao();
    boolean errorFlag = false;
    try {
      dao.masterCsvAll();
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * マスターデータ選択登録の場合.
   */
  @Test
  public void masterCsvRegistTest1() {
    MasterCsvDao dao = new MasterCsvDao();
    PropertiesLoad pl = new PropertiesLoad();
    Properties properties = pl.propertyLoad();
    String csvName = "m_menu.csv";
    File file = new File(properties.getProperty("csvFileDirectory") + csvName);
    String name = "m_menu";
    boolean errorFlag = false;
    try {
      dao.masterCsvRegist(file, name);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * csvファイルがnullの場合(デバック用テストケース).
   */
  @Ignore
  @Test
  public void masterCsvRegistTest2() {
    MasterCsvDao dao = new MasterCsvDao();
    String name = "m_menu";
    boolean errorFlag = false;
    try {
      dao.masterCsvRegist(null, name);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * ファイル名がnullの場合(デバック用テストケース).
   */
  @Ignore
  @Test
  public void masterCsvRegistTest3() {
    MasterCsvDao dao = new MasterCsvDao();
    PropertiesLoad pl = new PropertiesLoad();
    Properties properties = pl.propertyLoad();
    String csvName = "m_menu.csv";
    File file = new File(properties.getProperty("csvFileDirectory") + csvName);
    boolean errorFlag = false;
    try {
      dao.masterCsvRegist(file, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

}
