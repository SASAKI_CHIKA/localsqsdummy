package main.test.jp.co.dummy.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.dao.KeywordDao;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

/**
 * キーワード登録処理テスト.
 * @author vagrant
 *
 */
public class KeywordDaoTest {

  /**
   * 適切なクリエイティブID、キーワード名が入っている場合.
   */
  @Test
  public void keywordRegistTest() {
    KeywordDao dao = new KeywordDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int creativeId;
    String keywordName = "test";
    boolean errorFlag = false;

    String sql = "SELECT id FROM t_creative group by id desc";
    creativeId = vg.getNewId(sql);
    String id = vc.changeString(creativeId - 1);

    try {
      dao.keywordRegist(id, keywordName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * クリエイティブIDがnullの場合.
   */
  @Test
  public void keywordRegistTest2() {
    KeywordDao dao = new KeywordDao();
    String keywordName = "test";
    boolean errorFlag = false;

    try {
      dao.keywordRegist(null, keywordName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * キーワードがnullの場合.
   */
  @Test
  public void keywordRegistTest3() {
    KeywordDao dao = new KeywordDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int creativeId;
    boolean errorFlag = false;

    String sql = "SELECT id FROM t_creative group by id desc";
    creativeId = vg.getNewId(sql);
    String id = vc.changeString(creativeId - 1);
    try {
      dao.keywordRegist(id, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertFalse(errorFlag);
  }

  /**
   * クリエイティブID,キーワード共にnull
   */
  @Test
  public void keywordRegistTest4() {
    KeywordDao dao = new KeywordDao();
    boolean errorFlag = false;

    try {
      dao.keywordRegist(null, null);

    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

}
