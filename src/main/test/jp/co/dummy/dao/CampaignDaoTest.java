package main.test.jp.co.dummy.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import jp.co.dummy.dao.CampaignDao;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

/**
 * キャンペーン登録処理テスト.
 * 
 * @author vagrant
 *
 */
public class CampaignDaoTest {

  /**
   * 適切なアカウントID、キャンペーン名を入れた場合.
   */
  @Test
  public void campaignRegistTest1() {
    CampaignDao dao = new CampaignDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int accountId;
    String campaignName = "test";

    String sql = "SELECT id FROM t_account group by id desc;";
    accountId = vg.getNewId(sql);
    String id = vc.changeString(accountId - 1);

    int campaignId = dao.dbRegist(id, campaignName);
    assertNotNull(campaignId);
  }

  /**
   * アカウントIDがnullの場合.
   */
  @Test
  public void campaignRegistTest2() {
    CampaignDao dao = new CampaignDao();
    String campaignName = "test";
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, campaignName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * キャンペーン名がnullの場合.
   */
  @Test
  public void campaignRegistTest3() {
    CampaignDao dao = new CampaignDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int accountId;

    String sql = "SELECT id FROM t_account group by id desc;";
    accountId = vg.getNewId(sql);
    String id = vc.changeString(accountId - 1);
    int campaignId = dao.dbRegist(id, null);

    assertNotNull(campaignId);
  }

  /**
   * アカウントID,キャンペーン名共にnull
   */
  @Test
  public void campaignRegistTest4() {
    CampaignDao dao = new CampaignDao();
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, null);

    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

}
