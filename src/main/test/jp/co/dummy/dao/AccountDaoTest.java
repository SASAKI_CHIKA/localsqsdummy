package main.test.jp.co.dummy.dao;

import static org.junit.Assert.*;
import jp.co.dummy.dao.AccountDao;

import org.junit.Test;

/**
 * アカウント登録処理テスト.
 * @author vagrant
 *
 */
public class AccountDaoTest {

  /**
   * 期待値が返ってくる場合.
   * 
   * @throws Exception
   */
  @Test
  public void allRegistTest() {

    AccountDao dao = new AccountDao();
    int accountId = dao.dbRegist();
    assertNotNull(accountId);

  }

  /**
   * アカウント名を入れた場合.
   * 
   * @throws Exception
   */
  @Test
  public void accountRegistTest() {

    AccountDao dao = new AccountDao();
    String accountName = "test";
    int accountId = dao.dbRegist(accountName);
    assertNotNull(accountId);

  }

  /**
   * nullを入れた場合.
   */
  @Test
  public void accountRegistTest2() {

    AccountDao dao = new AccountDao();
    String accountName = null;
    int accountId = dao.dbRegist(accountName);
    assertNotNull(accountId);

  }
}
