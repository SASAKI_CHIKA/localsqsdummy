package main.test.jp.co.dummy.dao;

import static org.junit.Assert.*;
import jp.co.dummy.dao.AdGroupDao;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

import org.junit.Test;

/**
 * 広告グループ登録処理テスト.
 * @author vagrant
 *
 */
public class AdGroupDaoTest {

  /**
   * 既存のキャンペーンIDと適切な広告グループ名の場合.
   * 
   * @throws Exception
   */
  @Test
  public void adGroupRegistTest() throws Exception {

    AdGroupDao dao = new AdGroupDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int campaignId;
    String adGroupName = "test";

    String sql = "select id from t_campaign ORDER BY id desc LIMIT 1;";
    campaignId = vg.getNewId(sql);
    String id = vc.changeString(campaignId - 1);

    int adGroupId = dao.dbRegist(id, adGroupName);
    assertNotNull(adGroupId);

  }

  /**
   * キャンペーンIDがnullの場合.
   */
  @Test
  public void adGroupRegistTest2() {
    AdGroupDao dao = new AdGroupDao();
    String adGroupName = "test";
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, adGroupName);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }

  /**
   * 広告グループ名がnullの場合.
   * 
   * @throws Exception
   */
  @Test
  public void adGroupRegistTest3() throws Exception {

    AdGroupDao dao = new AdGroupDao();
    ValueGet vg = new ValueGet();
    ValueChange vc = new ValueChange();
    int campaignId;

    String sql = "select id from t_campaign ORDER BY id desc LIMIT 1;";
    campaignId = vg.getNewId(sql);
    String id = vc.changeString(campaignId - 1);

    int adGroupId = dao.dbRegist(id, null);
    assertNotNull(adGroupId);
  }

  /**
   * キャンペーンID、広告グループ共にnullの場合.
   */
  @Test
  public void adGroupRegistTest4() {
    AdGroupDao dao = new AdGroupDao();
    boolean errorFlag = false;

    try {
      dao.dbRegist(null, null);
    } catch (Exception e) {
      errorFlag = true;
    }
    assertTrue(errorFlag);
  }
}
