package jp.co.dummy.dao;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.util.DataAccess;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 広告グループ登録実装.
 * 
 * @author vagrant
 *
 */
public class AdGroupDao extends RegistBaseDao {

  /**
   * 広告グループ登録.
   */
  public int dbRegist(String campaignId, String adGroupName) {

    if (StringUtils.isEmpty(adGroupName)) {
      adGroupName = Constant.DEFAULT_NAME;
    }

    String result = null;
    int adGroupId = 0;
    int accountId;
    int id = vc.changeFigure(campaignId);

    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      // 新規ID取得
      String sql = "select id from t_ad_group ORDER BY id desc LIMIT 1;";
      adGroupId = vg.getNewId(sql);

      // アカウントID取得
      List<Map<String, Object>> list =
          jdbcTemplate.queryForList("SELECT account_id FROM t_campaign WHERE id = ?", id);
      for (Map<String, Object> map : list) {
        result = (map.get("account_id").toString());
      }
      accountId = Integer.parseInt(result);

      // データセット
      String sql3 =
          "INSERT INTO t_ad_group "
              + "(id,campaign_id,account_id,ad_group_name,default_price,"
              + "rotation_setting,creative_type,delivery_status) "
              + "VALUES (?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql3, new Object[]
      {adGroupId, id, accountId, adGroupName, Constant.DEFAULT_VALUE,
          Constant.ZERO, Constant.ZERO, Constant.ZERO});

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }

    return adGroupId;
  }

}
