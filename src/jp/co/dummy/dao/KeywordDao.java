package jp.co.dummy.dao;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.util.DataAccess;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * キーワード登録実装.
 * @author vagrant
 *
 */
public class KeywordDao extends RegistBaseDao {

  /**
   * キーワード登録.
   * @param creativeId
   * @param keywordName
   */
  public void keywordRegist(String creativeId, String keywordName) {

    if (StringUtils.isEmpty(keywordName)) {
      keywordName = Constant.DEFAULT_NAME;
    }

    int keywordId;
    int accountId;
    int campaignId;
    int adGroupId;
    int id = vc.changeFigure(creativeId);
    String result = null;
    String result2 = null;
    String result3 = null;

    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      // 新規ID取得
      String sql = "select id from t_purchased_keyword ORDER BY id desc LIMIT 1;";
      keywordId = vg.getNewId(sql);

      // アカウントID取得
      List<Map<String, Object>> list =
          jdbcTemplate.queryForList("SELECT * FROM t_creative WHERE id = ?;", id);
      for (Map<String, Object> map : list) {
        result = (map.get("account_id").toString());
        result2 = (map.get("campaign_id").toString());
        result3 = (map.get("ad_group_id").toString());
      }
      accountId = Integer.parseInt(result);
      campaignId = Integer.parseInt(result2);
      adGroupId = Integer.parseInt(result3);

      // データセット
      String sql2 =
          "INSERT INTO t_purchased_keyword "
              + "(id,account_id,campaign_id,ad_group_id,"
              + "purchased_keyword,word_matching_type,keyword_adjust,"
              + "keyword_morpheme,delivery_status,material_judgement_status) "
              + "VALUES (?,?,?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql2, new Object[]
      {keywordId, accountId, campaignId, adGroupId,
          keywordName, Constant.TWO, Constant.DEFAULT_NAME,
          Constant.DEFAULT_NAME, Constant.ZERO, Constant.TWO});

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }
  }

}
