package jp.co.dummy.dao;

import java.util.Calendar;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.util.DataAccess;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * キャンペーン登録実装.
 * 
 * @author vagrant
 *
 */
public class CampaignDao extends RegistBaseDao {

  /**
   * キャンペーン登録.
   */
  public int dbRegist(String accountId, String campaignName) {

    if (StringUtils.isEmpty(campaignName)) {
      campaignName = Constant.DEFAULT_NAME;
    }
    int id = vc.changeFigure(accountId);
    int campaignId = 0;
    c2.add(Calendar.MONTH, 1);

    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      // 新規ID取得
      String sql = "select id from t_campaign ORDER BY id desc LIMIT 1;";
      campaignId = vg.getNewId(sql);

      // データセット
      String sql2 =
          "INSERT INTO t_campaign "
              + "(id,account_id,campaign_name,target_article_type,"
              + "delivery_device_type,daily_budget_amount,delivery_type,"
              + "linked_type,docomo_flag,au_flag,softbank_flag,campaign_start_date,"
              + "campaign_end_date,delivery_status) "
              + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql2, new Object[]
      {campaignId, id, campaignName, Constant.ZERO, Constant.ONE,
          Constant.DEFAULT_VALUE, Constant.ONE, Constant.ZERO,
          Constant.ZERO, Constant.ZERO, Constant.ZERO, start, finish, Constant.ZERO});

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }

    return campaignId;
  }

}
