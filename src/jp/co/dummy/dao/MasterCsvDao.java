package jp.co.dummy.dao;

import java.io.File;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.util.DataAccess;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * マスターデータ登録処理.
 * 
 * @author vagrant
 *
 */
public class MasterCsvDao extends RegistBaseDao {

  /**
   * マスターデータ全登録.
   */
  public void masterCsvAll() {
    try {

      String path = properties.getProperty("csvFileDirectory");
      File dir = new File(path);
      File[] files = dir.listFiles();
      for (int i = 0; i < files.length; i++) {
        String name = files[i].toString();
        name = name.replaceAll(properties.getProperty("csvFileDirectory"), "");
        name = name.replaceAll(".csv", "");
        masterCsvRegist(files[i], name);
      }
    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }
  }

  /**
   * マスターデータ選択登録.
   * 
   * @param file
   * @param name
   */
  public void masterCsvRegist(File file, String name) {
    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      String sql =
          "LOAD DATA LOCAL INFILE "
              + "'"
              + file
              + "'"
              + " REPLACE INTO TABLE "
              + name
              + " FIELDS TERMINATED BY ',' ESCAPED BY '\\\\' ENCLOSED BY '\"' LINES TERMINATED BY '\r\n'IGNORE 1 LINES;";

      jdbcTemplate.update(sql);

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }
  }
}
