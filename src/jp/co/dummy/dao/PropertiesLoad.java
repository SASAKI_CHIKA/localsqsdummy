package jp.co.dummy.dao;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author vagrant
 *
 */
public class PropertiesLoad {

  /**
   * プロパティファイルのロード.
   * @return
   */
  public Properties propertyLoad() {

    final Logger logger = Logger.getLogger("Properties");
    Properties properties = new Properties();

    String fileName = "src/main/resource/delivery.conf";

    try {
      InputStream inputStream = new FileInputStream(fileName);
      properties.load(inputStream);

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }
    return properties;
  }
}
