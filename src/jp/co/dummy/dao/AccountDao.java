package jp.co.dummy.dao;

import java.util.Calendar;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.util.DataAccess;
import jp.co.dummy.util.ValueChange;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * アカウント登録実装クラス.
 * 
 * @author vagrant
 *
 */
public class AccountDao extends RegistBaseDao {

  public int dbRegist() {

    CampaignDao cam = new CampaignDao();
    AdGroupDao ad = new AdGroupDao();
    CreativeDao cre = new CreativeDao();
    KeywordDao key = new KeywordDao();

    int accountId;
    int campaignId;
    int adGroupId;
    int creativeId;
    String id;
    ValueChange vc = new ValueChange();

    accountId = dbRegist(Constant.DEFAULT_NAME);
    id = vc.changeString(accountId);
    campaignId = cam.dbRegist(id, Constant.DEFAULT_NAME);
    id = vc.changeString(campaignId);
    adGroupId = ad.dbRegist(id, Constant.DEFAULT_NAME);
    id = vc.changeString(adGroupId);
    creativeId = cre.dbRegist(id, Constant.DEFAULT_NAME);
    id = vc.changeString(creativeId);
    key.keywordRegist(id, Constant.DEFAULT_NAME);

    return accountId;
  }

  public int dbRegist(String accountName) {

    if (StringUtils.isEmpty(accountName)) {
      accountName = Constant.DEFAULT_NAME;
    }
    int accountId = 0;
    c2.add(Calendar.MONTH, 6);
    String encryptionId = vg.getLoginId();

    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      // 新規ID取得(アカウントID)
      String sql = "SELECT id FROM t_account ORDER BY id desc LIMIT 1;";
      accountId = vg.getNewId(sql);

      String hashCode = vc.hash(Constant.LOGIN);
      // String encryptionId = vc.encryption(loginId);

      // t_account_application
      sql =
          "INSERT INTO t_account_application "
              + "(id,primary_ad_agency_id,secondary_ad_agency_id,"
              + "advertiser_name_applied,advertiser_id,site_name,"
              + "judgement_industry_id_applied,pc_url,encrypted_contact_mail_address,"
              + "monthly_budget_amount,ad_start_date,ad_end_date,"
              + "material_judgement_automatic_delivery_type,application_status,application_user_id,"
              + "judgement_status,judgement1st_status,judgement2nd_user_id,"
              + "judgement2nd_status,account_registration_user_id,manager_approval_user_id,manager_approval_status) "
              + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql, new Object[]
      {accountId, Constant.AGENCY, Constant.AGENCY2,
          Constant.ADVERTISER, Constant.ADVERTISER_ID, accountName,
          Constant.INDUSTRY_ID, properties.getProperty("defaultUrl"), encryptionId,
          Constant.AMOUNT, start, finish,
          Constant.ONE, Constant.ONE, Constant.APP_USER_ID,
          Constant.FOUR, Constant.TREE, Constant.JUDGE_USER_ID,
          Constant.TWO, Constant.JUDGE_USER_ID, Constant.JUDGE_USER_ID, Constant.ONE});

      // t_account
      String sql2 =
          "INSERT INTO t_account "
              + "(id,primary_ad_agency_id,secondary_ad_agency_id,advertiser_id,site_name,"
              + "judgement_industry_id,sales_analysis_industry_id,pc_url,ad_start_date,"
              + "ad_end_date,material_judgement_automatic_delivery_type) "
              + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";

      jdbcTemplate.update(sql2, new Object[]
      {accountId, Constant.AGENCY, Constant.AGENCY2, Constant.ADVERTISER_ID, accountName,
          Constant.INDUSTRY_ID, Constant.SALES_INDUSTRY, properties.getProperty("defaultUrl"), start,
          finish, Constant.ONE});


      // m_system_user
      String sql3 =
          "INSERT INTO m_system_user "
              + "(encrypted_login_id,company_id,role_id,password_hash,"
              + "password_expiration_date,system_user_type,encrypted_user_name,"
              + "encrypted_contact_mail_address,alert_notification_type,system_user_status) "
              + "VALUES (?,?,?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql3, new Object[] {encryptionId, Constant.CONPANY_ID, Constant.ROLE_ID, hashCode,
          finish, Constant.ZERO, hashCode, hashCode,
          Constant.ZERO, Constant.ZERO});

      // t_account_monthly_budget
      String sql4 = "INSERT INTO t_account_monthly_budget "
          + "(account_id,year_and_month,budget_amount) "
          + "VALUES (?,?,?);";

      jdbcTemplate.update(sql4, new Object[]
      {accountId, month, Constant.AMOUNT});


      // t_account_judgement_item
      String sql5 = "INSERT INTO t_account_judgement_item "
          + "(account_id) VALUES (?);";

      jdbcTemplate.update(sql5, new Object[] {accountId});

      // t_account_budget_used_circumstance
      String sql6 = "INSERT INTO t_account_budget_used_circumstance "
          + "(account_id,receive_month) "
          + "VALUES (?,?);";

      jdbcTemplate.update(sql6, new Object[] {accountId, month});

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }
    return accountId;
  }

}
