package jp.co.dummy.dao;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import jp.co.dummy.base.RegistBaseDao;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.util.DataAccess;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * クリエイティブ登録実装.
 * 
 * @author vagrant
 *
 */
public class CreativeDao extends RegistBaseDao {

  /**
   * クリエイティブ登録.
   */
  public int dbRegist(String adGroupId, String creativeName) {

    if (StringUtils.isEmpty(creativeName)) {
      creativeName = Constant.DEFAULT_NAME;
    }

    int creativeId = 0;
    int accountId;
    int campaignId;
    int id = vc.changeFigure(adGroupId);
    String result = null;
    String result2 = null;

    try {
      jpaConfig = new DataAccess();
      jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

      // 新規ID取得
      String sql = "select id from t_creative ORDER BY id desc LIMIT 1;";
      creativeId = vg.getNewId(sql);

      // アカウントID取得
      List<Map<String, Object>> list =
          jdbcTemplate.queryForList("SELECT * FROM t_ad_group WHERE id = ?;", id);
      for (Map<String, Object> map : list) {
        result = (map.get("account_id").toString());
        result2 = (map.get("campaign_id").toString());
      }
      accountId = Integer.parseInt(result);
      campaignId = Integer.parseInt(result2);

      // データセット t_creative
      String sql3 =
          "INSERT INTO t_creative "
              + "(id,ad_group_id,campaign_id,account_id,"
              + "creative_name,creative_type,link_url,"
              + "delivery_status,material_judgement_status) "
              + "VALUES (?,?,?,?,?,?,?,?,?);";

      jdbcTemplate.update(sql3, new Object[]
      {creativeId, id, campaignId, accountId,
          creativeName, Constant.ZERO, properties.getProperty("defaultUrl"),
          Constant.ZERO, Constant.TWO});

      // データセット t_text_creative
      String sql4 =
          "INSERT INTO t_text_creative "
              + "(id,title,description1,display_url) "
              + "VALUES (?,?,?,?);";

      jdbcTemplate.update(sql4, new Object[]
      {creativeId, Constant.DEFAULT_NAME, Constant.DEFAULT_NAME, properties.getProperty("defaultUrl")});

    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    }

    return creativeId;
  }

}
