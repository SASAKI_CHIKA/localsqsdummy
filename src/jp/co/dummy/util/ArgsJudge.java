package jp.co.dummy.util;

import java.util.List;
import java.util.logging.Logger;

import jp.co.dummy.constant.Constant;
import jp.co.dummy.regist.AccountRegistration;
import jp.co.dummy.regist.AdGroupRegistration;
import jp.co.dummy.regist.CampaignRegistration;
import jp.co.dummy.regist.CreativeRegistration;
import jp.co.dummy.regist.KeywordRegistration;
import jp.co.dummy.regist.MasterCsvReader;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author vagrant
 *
 */
public class ArgsJudge {

  /**
   * 引数のnull,空文字判定.
   * 
   * @param args
   * @return
   */
  public boolean argsJudge(String args) {
    if (StringUtils.isEmpty(args)) {
      return true;
    }
    return false;
  }

  /**
   * インスタンス生成.
   * 
   * @param list
   * @return
   */
  public Object createInstance(List<String> list) {

    final Logger logger = Logger.getLogger("ArgsJudge");
    Object obj = null;

    try {

      if (StringUtils.equals("master", list.get(0))) {
        obj = MasterCsvReader.class.newInstance();
        return obj;
      } else if (list.get(0).endsWith(".csv")) {
        obj = MasterCsvReader.class.getConstructor(String.class).newInstance(list.get(0));
        return obj;
      }

      if (StringUtils.equals("all", list.get(0))) {
        obj = AccountRegistration.class.newInstance();
        return obj;
      } else if (StringUtils.equals("account", list.get(0))) {

        if (list.size() == 2) {

          obj = AccountRegistration.class.getConstructor(String.class).newInstance(list.get(1));
          return obj;
        } else {
          list.add(Constant.DEFAULT_NAME);
          obj = AccountRegistration.class.getConstructor(String.class).newInstance(Constant.DEFAULT_NAME);
          return obj;
        }
      }

      if (StringUtils.equals("campaign", list.get(0)) && StringUtils.isNotEmpty(list.get(1))) {
        if (list.size() == 3) {
          obj =
              CampaignRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  list.get(2));
          return obj;
        } else {
          list.add(Constant.DEFAULT_NAME);
          obj =
              CampaignRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  Constant.DEFAULT_NAME);
          return obj;
        }
      }

      if (StringUtils.equals("ad_group", list.get(0)) && StringUtils.isNotEmpty(list.get(1))) {
        if (list.size() == 3) {
          obj =
              AdGroupRegistration.class.getConstructor(String.class, String.class)
                  .newInstance(list.get(1), list.get(2));
          return obj;
        } else {
          list.add(Constant.DEFAULT_NAME);
          obj =
              AdGroupRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  Constant.DEFAULT_NAME);
          return obj;
        }
      }

      if (StringUtils.equals("creative", list.get(0)) && StringUtils.isNotEmpty(list.get(1))) {
        if (list.size() == 3) {
          obj =
              CreativeRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  list.get(2));
          return obj;
        } else {
          list.add(Constant.DEFAULT_NAME);
          obj =
              CreativeRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  Constant.DEFAULT_NAME);
          return obj;
        }
      }

      if (StringUtils.equals("keyword", list.get(0)) && StringUtils.isNotEmpty(list.get(1))) {
        if (list.size() == 3) {
          obj =
              KeywordRegistration.class.getConstructor(String.class, String.class)
                  .newInstance(list.get(1), list.get(2));
          return obj;
        }
        else {
          list.add(Constant.DEFAULT_NAME);
          obj =
              KeywordRegistration.class.getConstructor(String.class, String.class).newInstance(list.get(1),
                  Constant.DEFAULT_NAME);
          return obj;
        }
      }

    } catch (Exception e) {
      logger.warning(e.getMessage());
    }
    return obj;
  }
}
