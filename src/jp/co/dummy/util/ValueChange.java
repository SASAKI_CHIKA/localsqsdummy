package jp.co.dummy.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 値の変換.
 * 
 * @author vagrant
 *
 */
public class ValueChange {

  /**
   * 文字列へ変換.
   * 
   * @param figure
   * @return
   */
  public String changeString(int figure) {
    String result;
    result = String.valueOf(figure);
    return result;
  }

  /**
   * 数字へ変換.
   * 
   * @param string
   * @return
   */
  public int changeFigure(String str) {
    int result;
    result = Integer.parseInt(str);
    return result;
  }

  /**
   * 文字列のハッシュ化.
   * 
   * @param text
   * @return
   * @throws NoSuchAlgorithmException
   */
  public String hash(String text) throws NoSuchAlgorithmException {
    String plainText = text;
    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(plainText.getBytes());
    byte[] hashBytes = md.digest();
    int[] hashInts = new int[hashBytes.length];
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < hashBytes.length; i++) {
      hashInts[i] = (int) hashBytes[i] & 0xff;
      if (hashInts[i] <= 15) {
        sb.append("0");
      }
      sb.append(Integer.toHexString(hashInts[i]));
    }
    String hashText = sb.toString();
    return hashText;
  }
}
