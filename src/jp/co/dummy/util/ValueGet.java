package jp.co.dummy.util;

import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 値の取得.
 * 
 * @author vagrant
 *
 */
public class ValueGet {

  /**
   * 新規ID取得.
   * 
   * @param sql
   * @return
   */
  public int getNewId(String sql) {
    
    Logger logger = Logger.getLogger("ValueGet");
    DataAccess jpaConfig;
    JdbcTemplate jdbcTemplate;
    jpaConfig = new DataAccess();
    jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

    String result = null;
    int maxId = 1;
    try{

    List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
    for (Map<String, Object> map : list) {
      result = (map.get("id").toString());
      break;
    }
    maxId += Integer.parseInt(result);
    }catch(Exception e){
      logger.log(Level.WARNING, "例外発生", e);
    }
    return maxId;
  }


  /**
   * loginIdの振り分け.
   * 
   * @return
   */
  public String getLoginId() {

    int loop = 0;
    String loginId = null;
    String result = null;
    int resultValue;
    String sql = "EXISTS(SELECT 1 FROM m_system_user WHERE encrypted_login_id ='";
    String sql2 = "')";
    String encryptedText;

    DataAccess jpaConfig;
    JdbcTemplate jdbcTemplate;
    jpaConfig = new DataAccess();
    jdbcTemplate = new JdbcTemplate(jpaConfig.dataSource());

    do {
      loop++;
      loginId = RandomStringUtils.randomAlphanumeric(loop);
      
      //暗号化
      encryptedText = Base64.getUrlEncoder().encodeToString(loginId.getBytes());

      List<Map<String, Object>> list =
          jdbcTemplate.queryForList("SELECT EXISTS(SELECT 1 FROM m_system_user WHERE encrypted_login_id =?);", encryptedText);

      for (Map<String, Object> map : list) {
        result = (map.get(sql + encryptedText + sql2).toString());
      }
      resultValue = Integer.parseInt(result);
    } while (resultValue != 0);

    return encryptedText;
  }

}
