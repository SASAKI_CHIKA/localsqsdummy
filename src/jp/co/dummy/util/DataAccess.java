package jp.co.dummy.util;

import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import jp.co.dummy.dao.PropertiesLoad;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * DB接続処理.
 * 
 * @author vagrant
 *
 */
@Configuration
public class DataAccess {

  @Bean(name = "dataSource")
  public DataSource dataSource() {

    // プロパティファイル取得
    PropertiesLoad pl = new PropertiesLoad();
    Properties properties = pl.propertyLoad();

    // コネクションの取得
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(properties.getProperty("className"));
    dataSource.setUrl(properties.getProperty("databaseUrl"));
    dataSource.setUsername(properties.getProperty("databaseUser"));
    dataSource.setPassword(properties.getProperty("databasePass"));

    // トランザクション分離レベル(確定した最新データを常に読み取る)
    dataSource.setDefaultTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

    return dataSource;
  }

  @Autowired
  @Bean
  // トランザクション制御
  public PlatformTransactionManager transactionManager(final DataSource dataSource) {
    // ログの出力
    DataSourceTransactionManager txManager = new DataSourceTransactionManager();
    txManager.setDataSource(dataSource);

    return txManager;
  }
}
