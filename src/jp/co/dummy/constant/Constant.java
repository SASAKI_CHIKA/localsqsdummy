package jp.co.dummy.constant;

/**
 * 定数指定.
 * @author vagrant
 *
 */
public class Constant {

  public static final String DEFAULT_NAME = "テスト";

  public static final int ZERO = 0;
  public static final int ONE = 1;
  public static final int TWO = 2;
  public static final int TREE = 3;
  public static final int FOUR = 4;
  public static final int DEFAULT_VALUE = 100;
  
  public static final int AGENCY = 1000001;
  public static final int AGENCY2 = 2000003;
  public static final int SALES_INDUSTRY = 67025;
  public static final int AMOUNT = 100000;
  public static final int ROLE_ID = 11;
  public static final int CONPANY_ID = 3000014;
  public static final String LOGIN = "test";
  public static final int INDUSTRY_ID = 98778;
  public static final String ADVERTISER = "D2C";
  public static final int ADVERTISER_ID = 3000014;
  public static final int APP_USER_ID = 200000301;
  public static final int JUDGE_USER_ID = 100000020;

}
