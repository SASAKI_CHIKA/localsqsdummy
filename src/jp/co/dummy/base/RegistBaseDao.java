package jp.co.dummy.base;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Logger;

import org.springframework.jdbc.core.JdbcTemplate;

import jp.co.dummy.dao.PropertiesLoad;
import jp.co.dummy.util.DataAccess;
import jp.co.dummy.util.ValueChange;
import jp.co.dummy.util.ValueGet;

public abstract class RegistBaseDao {

  protected PropertiesLoad pl = new PropertiesLoad();
  protected Properties properties = pl.propertyLoad();

  protected ValueChange vc = new ValueChange();
  protected ValueGet vg = new ValueGet();
  protected DataAccess jpaConfig;
  protected JdbcTemplate jdbcTemplate;

  // 日付取得
  protected Calendar c = Calendar.getInstance();
  protected Calendar c2 = Calendar.getInstance();
  protected SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  protected SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMM");
  protected String start = (sdf.format(c.getTime()));
  
  protected String finish = (sdf.format(c2.getTime()));
  protected String month = (sdf2.format(c2.getTime()));

  protected Logger logger = Logger.getLogger("RegistBaseDao");

  /**
   * 登録処理実装.
   * 
   * @return
   * @throws Exception
   */
  protected int dbRegist() {
    int resultId = 0;
    return resultId;
  }

  /**
   * 登録処理実装.
   * 
   * @param name
   * @return
   */
  protected int dbRegist(String name) {
    int resultId = 0;
    return resultId;
  }

  /**
   * 登録処理実装.
   * 
   * @param id
   * @param name
   * @return
   */
  public int dbRegist(String id, String name) {
    int resultId = 0;
    return resultId;
  }
}
