package jp.co.dummy.base;

import java.util.logging.Logger;

public abstract class RegistDelivery {
  
  public Logger logger = Logger.getLogger("RegistDelivery");

  /**
   * 登録処理への受け渡し.
   */
  protected void registDelivery() {}

  /**
   * 登録処理への受け渡し.
   * 
   * @param accountName
   * @return
   * @throws Exception
   */
  protected void registDelivery(String name) {}

  /**
   * 登録処理への受け渡し.
   * 
   * @param id
   * @param name
   */
  protected void registDelivery(String id, String name) {}

}
