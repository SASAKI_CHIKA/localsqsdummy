package jp.co.dummy.main;

import jp.co.dummy.dao.PropertiesLoad;
import jp.co.dummy.util.ArgsJudge;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.SendMessageRequest;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author vagrant
 *
 */
public class LocalDummyData {
  /**
   * 
   * @param args
   */
  public static void main(String[] args) {

    boolean result;
    Object instanceName;
    final Logger logger = Logger.getLogger("LocalDummyData");

    try {
      // 引数リストに追加
      List<String> list = new ArrayList<String>();
      for (int i = 0; i < args.length; i++) {
        list.add(args[i]);
      }

      // 空文字判定
      ArgsJudge judge = new ArgsJudge();
      result = judge.argsJudge(list.get(0));
      if (result) {
        logger.warning("引数を正しく設定してください。");
        return;
      }

      // ヘルプ情報を出す
      if (StringUtils.equals("--help", args[0])) {

      }
      // インスタンス生成
      instanceName = judge.createInstance(list);

      if (StringUtils.equals("master", list.get(0)) || StringUtils.equals("all", list.get(0))) {
        // 引数なし
        Method method = instanceName.getClass().getMethod("registDelivery");
        method.invoke(instanceName);

      } else if (list.get(0).endsWith(".csv")) {

        Class<?> para[] = new Class<?>[] {String.class};
        Method m = instanceName.getClass().getMethod("registDelivery", para);
        m.invoke(instanceName, list.get(0));

      } else if (StringUtils.equals("account", list.get(0))) {
        Class<?> para[] = new Class<?>[] {String.class};
        Method m = instanceName.getClass().getMethod("registDelivery", para);
        m.invoke(instanceName, list.get(1));

      } else {
        Class<?> para[] = new Class<?>[] {String.class, String.class};
        Method m = instanceName.getClass().getMethod("registDelivery", para);
        m.invoke(instanceName, list.get(1), list.get(2));

      }

    } catch (IndexOutOfBoundsException e) {
      logger.warning("引数を正しく設定してください。");
    } catch (Exception e) {
      logger.log(Level.WARNING, "例外発生", e);
    } finally {
      // キュー送信
      PropertiesLoad pl = new PropertiesLoad();
      Properties properties = pl.propertyLoad();
      AmazonSQS client = new AmazonSQSClient(new BasicAWSCredentials("dummyAKey", "dummySKey"));
      client.setEndpoint(properties.getProperty("queue.endpoint"));
      client.sendMessage(new SendMessageRequest(properties.getProperty("queue.url"), "messageBody"));

    }
  }
}
