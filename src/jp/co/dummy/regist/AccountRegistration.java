package jp.co.dummy.regist;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.dao.AccountDao;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author vagrant アカウント登録.
 *
 */
public class AccountRegistration extends RegistDelivery {

  AccountDao dao = new AccountDao();
  public String accountName;
  public int accountId;

  public AccountRegistration() {}

  /**
   * 
   * @param select
   * @param accountName
   */
  public AccountRegistration(String accountName) {
    this.accountName = accountName;
  }

  /**
   * アカウントからキーワードまで登録.
   * 
   * @return
   * @throws Exception
   */
  public void registDelivery() {
    accountId = dao.dbRegist();
    logger.info("新規登録アカウントID：" + accountId);
  }

  /**
   * アカウント登録.
   * 
   * @param accountName
   * @return
   */
  public void registDelivery(String accountName) {
    if (StringUtils.isEmpty(accountName)) {
      accountName = Constant.DEFAULT_NAME;
    }
    accountId = dao.dbRegist(accountName);
    logger.info("新規登録アカウントID：" + accountId);
  }

}
