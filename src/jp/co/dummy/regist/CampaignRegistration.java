package jp.co.dummy.regist;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.dao.CampaignDao;

/**
 * 
 * @author vagrant キャンペーン登録.
 *
 */
public class CampaignRegistration extends RegistDelivery {

  CampaignDao dao = new CampaignDao();
  public String id;
  public String campaignName;
  public int campaignId;

  /**
   * 
   * @param id
   * @param name
   */
  public CampaignRegistration(String id, String campaignName) {
    this.id = id;
    this.campaignName = campaignName;
  }

  /**
   * キャンペーン登録.
   * 
   * @param id
   * @param campaignName
   * @return
   */
  public void registDelivery(String id, String campaignName) {
    campaignId = dao.dbRegist(id, campaignName);
    logger.info("新規キャンペーンID：" + campaignId);
  }

}
