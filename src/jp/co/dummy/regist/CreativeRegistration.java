package jp.co.dummy.regist;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.dao.CreativeDao;

/**
 * 
 * @author vagrant クリエイティブ登録.
 *
 */
public class CreativeRegistration extends RegistDelivery {

  CreativeDao dao = new CreativeDao();
  public String id;
  public String creativeName;
  public int creativeId;

  /**
   * 
   * @param id
   * @param name
   */
  public CreativeRegistration(String id, String creativeName) {
    this.id = id;
    this.creativeName = creativeName;
  }

  /**
   * クリエイティブ登録.
   * 
   * @param id
   * @param creativeName
   * @return
   */
  public void registDelivery(String id, String creativeName) {
    creativeId = dao.dbRegist(id, creativeName);
    logger.info("新規クリエイティブID：" + creativeId);
  }

}
