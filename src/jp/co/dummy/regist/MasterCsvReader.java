package jp.co.dummy.regist;

import java.io.File;
import java.util.Properties;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.dao.MasterCsvDao;
import jp.co.dummy.dao.PropertiesLoad;

/**
 * 
 * @author vagrant マスターデータを投入する.
 *
 */
public class MasterCsvReader extends RegistDelivery {

  PropertiesLoad pl = new PropertiesLoad();
  Properties properties = pl.propertyLoad();
  MasterCsvDao dao = new MasterCsvDao();
  public String csvName;

  public MasterCsvReader() {}

  /**
   * 
   * @param csvName
   */
  public MasterCsvReader(String csvName) {
    this.csvName = csvName;
  }

  /**
   * マスターデータ全登録.
   */
  public void dbRegist() {
    dao.masterCsvAll();
  }

  /**
   * マスターデータ指定登録.
   * 
   * @param csvName
   * @throws Exception
   */
  public void registDelivery(String csvName) {
    File file = new File(properties.getProperty("csvFileDirectory") + csvName);
    String name = csvName.replaceAll(".csv", "");
    dao.masterCsvRegist(file, name);
  }
}
