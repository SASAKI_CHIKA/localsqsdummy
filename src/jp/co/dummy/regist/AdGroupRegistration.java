package jp.co.dummy.regist;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.constant.Constant;
import jp.co.dummy.dao.AdGroupDao;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @author vagrant 広告グループ登録.
 *
 */
public class AdGroupRegistration extends RegistDelivery {

  AdGroupDao dao = new AdGroupDao();
  public String id;
  public String adGroupName;
  public int adGroupId;

  /**
   * 
   * @param id
   * @param name
   */
  public AdGroupRegistration(String id, String adGroupName) {
    this.id = id;
    this.adGroupName = adGroupName;
  }

  /**
   * 広告グループ登録.
   * 
   * @param id
   * @param adGroupName
   * @return
   */
  public void registDelivery(String id, String adGroupName) {
    if (StringUtils.isEmpty(adGroupName)) {
      adGroupName = Constant.DEFAULT_NAME;
    }
    adGroupId = dao.dbRegist(id, adGroupName);
    logger.info("新規広告グループID：" + adGroupId);
  }


}
