package jp.co.dummy.regist;

import jp.co.dummy.base.RegistDelivery;
import jp.co.dummy.dao.KeywordDao;

/**
 * 
 * @author vagrant キーワード登録.
 *
 */
public class KeywordRegistration extends RegistDelivery {

  KeywordDao dao = new KeywordDao();
  public String id;
  public String keywordName;

  /**
   * 
   * @param id
   * @param name
   */
  public KeywordRegistration(String id, String keywordName) {
    this.id = id;
    this.keywordName = keywordName;
  }

  /**
   * キーワード登録.
   * 
   * @param id
   * @param keywordName
   * @throws Exception
   */
  public void registDelivery(String id, String keywordName) {
    dao.keywordRegist(id, keywordName);
  }


}
